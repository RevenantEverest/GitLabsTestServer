const express = require('express');
const logger = require('morgan');
const methodOverride = require('method-override');
const bodyParser = require('body-parser');

const PORT = process.env.PORT || 3001;
const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/", (req, res) => {
  res.json({
    message: 'Doing something else'
  });
});

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
